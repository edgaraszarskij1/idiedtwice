import React, { Component, Fragment } from 'react';
import BoardDrawer from './boardDrawer'

let howMany = 0;

class Board extends Component {
  constructor(props) {
    super(props)
    let newArr = []

    for (var i = 0; i < 3; i++) {
      newArr[i] = [];
      for (var j = 0; j < 3; j++) {
        howMany++
        newArr[i][j] = howMany
      }
    }

    this.state = ({ newArr, turns: 0, sorted: false })
    this.handleClick = this.handleClick.bind(this)
  }

  newGameHandler = () => {
    let newArr = this.state.newArr;
    howMany = 0
    for (var i = 0; i < 3; i++) {
      newArr[i] = [];
      for (var j = 0; j < 3; j++) {
        howMany++
        newArr[i][j] = howMany
      }
    }
    this.setState({ newArr, turns: 0, sorted: false })
  }

  isEnded = (arr) => {
    for (let i = 0; i < arr.length; i++) {
      if (arr[0][i] === arr[1][i] && arr[1][i] === arr[2][i] && arr[0][i] === arr[2][i]) return true
      if (arr[i][0] === arr[i][1] && arr[i][1] === arr[i][2] && arr[i][0] === arr[i][2]) return true
    } return false
  }

isDiagonal = (arr) => {
  let x = 0;
    for (let i = 0; i< arr.length; i++){
     if(arr[i][i]!=='x' && arr[i][i]!=='o' ) break;
      x+=arr[i][i]==='x'?1:-1;
    }
    return Math.abs(x) === 3;
}

  isDiagonal1 = (arr) => {
    let x = 0;
    for (let i = 0; i< arr.length; i++){
     if(arr[i][arr.length-i-1]!=='x' && arr[i][arr.length-i-1]!=='o' ) break;
      x+=arr[i][arr.length-i-1]==='x'?1:-1;
    }
    return Math.abs(x) === 3;
  }

  handleClick = (boardCell) => {

    if (this.isDiagonal1(this.state.newArr) || this.isDiagonal(this.state.newArr) || this.isEnded(this.state.newArr)) {
      this.setState({ sorted: true });
      return;
    }

    if (this.state.turns % 2 === 0) {

      this.setState({
        newArr: this.state.newArr.map((ro) => ro.map((el) => ((el === boardCell ? 'x' : el)))),
        turns: this.state.turns + 1,
      });
    }
    if (this.state.turns % 2 !== 0) {

      this.setState({
        newArr: this.state.newArr.map((ro) => ro.map((el) => ((el === boardCell ? 'o' : el)))),
        turns: this.state.turns + 1,
      });
    }
  }

  render() {
    return (
      <Fragment>
        <BoardDrawer tiles={this.state.newArr} turn={this.state.turns} handleClick={this.handleClick} />
        <button onClick={() => this.newGameHandler()} style={{ marginLeft: '625px' }}>New Game</button>
      </Fragment>
    )
  }
}

export default Board